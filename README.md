# Simple Trade Filter Utility
This AddOn aims to provide a simple interface for filtering global channel messages based on user-defined keywords.

## Usage
By default, the AddOn has no keywords, and the filter is disabled. 
A typical first time setup example is provided below. Here, keywords "boost" and "Boost" (note that the filter is case-sensitive) are added to the filter, and the filter is enabled.
```
/stfu add "boost"
/stfu add "Boost"
/stfu enable
```

The filter and enable status is globally saved, and may be accessed through commands `/stfu list` and `/stfu status`, respectively.
