-- First time initialization of shared variables
if stfu_enable_bool == nil then
	stfu_enable_bool = false
end
if stfu_keyword_table == nil then
	stfu_keyword_table = {}
end

-- Load filter on init if globally enabled
function stfuInit()
	if stfu_enable_bool == true then
		ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL", stfuFilter)
	end
end
local loginFrame = CreateFrame("Frame")
loginFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
loginFrame:SetScript("OnEvent", stfuInit)

-- Slash command handlers
-- Source: https://wowwiki.fandom.com/wiki/Creating_a_slash_command
local function MyAddonCommands(msg, editbox)
	local _, _, cmd, args = string.find(msg, "%s?(%w+)%s?(.*)")
	if cmd == "add" and args ~= "" then
		print("\124cffFFFF00Adding keyword '" .. args .. "'...\124r" )
		table.insert(stfu_keyword_table, args)
		print("\124cffFFFF00Keyword '" .. args .. "' successfully added to filter!")
	elseif cmd == "remove" and args ~= "" then
		print("\124cffFFFF00Removing keyword '" .. args .. "'...\124r" )
		status = stfuTableRemove(stfu_keyword_table, args)
		if status == true then
			print("\124cffFFFF00Keyword '" .. args .. "' successfully removed from filter!")
		else
			print("\124cffFF0000Keyword '" .. args .. "' not found in filter!")
		end
	elseif cmd == "enable" then
		ChatFrame_AddMessageEventFilter("CHAT_MSG_CHANNEL", stfuFilter)
		stfu_enable_bool = true
		print("\124cffFFFF00Filter enabled!")
	elseif cmd == "disable" then
		ChatFrame_RemoveMessageEventFilter("CHAT_MSG_CHANNEL", stfuFilter)
		stfu_enable_bool = false
		print("\124cffFFFF00Filter disabled!")
	elseif cmd == "status" then
		stfuStatus();
	elseif cmd == "list" then
		stfuList();
	else
		print("\124cffFFFF00Help:\124r")
		print("\124cffFFFF00-'/stfu enable' to enable filter\124r")
		print("\124cffFFFF00-'/stfu disable' to disable filter\124r")
		print("\124cffFFFF00-'/stfu add <keyword>' to add keyword to filter\124r")
		print("\124cffFFFF00-'/stfu remove <keyword>' to remove keyword from filter\124r")
		print("\124cffFFFF00-'/stfu list' to list current filtered words\124r")
		print("\124cffFFFF00-'/stfu status' to see if filter is enabled/disabled\124r")
	end
end
SLASH_STFU1, SLASH_STFU2 = '/stfu', '/tradefilter'
SlashCmdList["STFU"] = MyAddonCommands

-- List keywords
function stfuList()
	print("\124cffFFFF00Filtered keywords:\124r")
	for index, data in ipairs(stfu_keyword_table) do
		print(string.format("- %q", data))
	end
end

-- Show status
function stfuStatus()	
	print(string.format("\124cffFFFF00Filter enabled:\124r %s", tostring(stfu_enable_bool)))
end

-- Chat filter function
function stfuFilter(frame, event, message, sender, ...)
	for i, v in ipairs(stfu_keyword_table) do
		if message:find(v) or sender:find(v) then
			return true -- hide this message
		end
	end
end

-- Remove element of table
function stfuTableRemove(tab, val)
	for k,v in pairs(tab) do
		if(v==val) then
			table.remove(tab, k)
			return true
		end
	end
	return false
end
